# Eval Kubernetes RS

## Getting started

### Requirements :
- Multipass
- Ssh
- KVM/libvirt
- Ansible
- Ansible-Playbook
- ks0ctl
- terraform


Certains paramètres sont aussi à changer selon votre configuration, votre OS et votre $HOME.

## Avec Ansible Playbook :
https://docs.k0sproject.io/v1.21.3+k0s.0/examples/ansible-playbook/

```bash
git clone git@gitlab.com:M4ldoror/Eval-kubernetes-RS.git
cd Eval-kubernetes-RS
./tools/multipass_create_instances.sh 4
./tools/multipass_generate_inventory.py
cp /home/paco.garcia/k0s-ansible/tools/inventory.yml inventory/multipass/inventory.yml
```

### Inventory/multipass/inventory.yml :

```yaml
  ---
  all:
    children:
      initial_controller:
        hosts:
          pgarciaipi-1:
      controller:
        hosts:
          pgarciaipi-2:
      worker:
        hosts:
          pgarciaipi-3:
          pgarciaipi-4:
    hosts:
      pgarciaipi-1:
        ansible_host: 10.60.27.167
      pgarciaipi-2:
        ansible_host: 10.60.27.252
      pgarciaipi-3:
        ansible_host: 10.60.27.239
      pgarciaipi-4:
        ansible_host: 10.60.27.144

    vars:
      ansible_user: k0s


```

```bash
ansible -i inventory/multipass/inventory.yml -m ping all
ansible-playbook site.yml -i inventory/multipass/inventory.yml
export KUBECONFIG=/home/paco.garcia/k0s-ansible/inventory/multipass/artifacts/k0s-kubeconfig.yml
kubectl cluster-info
kubectl get nodes -o wide

```
## Avec k0sctl + multipass :
https://docs.k0sproject.io/v1.21.3+k0s.0/k0sctl-install/



```bash
git clone git@gitlab.com:M4ldoror/Eval-kubernetes-RS.git
cd Eval-kubernetes-RS
./tools/multipass_create_instances.sh 4

```

### Ajouter les IP dans le manifest k0sctl.yaml :

```yaml
apiVersion: k0sctl.k0sproject.io/v1beta1
kind: Cluster
metadata:
  name: pgarciaipi
spec:
  hosts:
  - role: controller
    ssh:
      address: 192.168.122.167 # replace with the controller's IP address
      user: k0s
      keyPath: ~/.ssh/id_rsa
  - role: worker
    ssh:
      address: 192.168.122.252 # replace with the controller's IP address
      user: k0s
      keyPath: ~/.ssh/id_rsa
  - role: worker
    ssh:
      address: 192.168.122.239 # replace with the controller's IP address
      user: k0s
      keyPath: ~/.ssh/id_rsa
  - role: worker
    ssh:
      address: 192.168.122.144 # replace with the controller's IP address
      user: k0s
      keyPath: ~/.ssh/id_rsa

```

### Lancer :

```bash
k0sctl apply --config k0sctl.yaml

```

ps : le cloud_init n'est pas le même que avec le deployement Ansible-playbook, voir fichier cloud_init.cfg


## Experimental : Avec Terraform et provider KVM dmacvicar :
https://github.com/dmacvicar/terraform-provider-libvirt

```bash
terraform init
terraform apply
```
Pour la suite suivre la procédure k0sctl : https://docs.k0sproject.io/v1.21.3+k0s.0/k0sctl-install/

# Backup :

export KUBECONFIG=/home/prenom.nom/Eval-kubernetes-RS/inventory/multipass/artifacts/k0s-kubeconfig.yml

### Avec k0s :

- Backup :

```bash
multipass shell pgarciaipi_1
sudo su
k0s backup --save-path=/tmp
```

- Restore :

```bash
multipass shell pgarciaipi_1
sudo su
k0s restore /tmp/k0s_backup_2021-04-26T19_51_57_000Z.tar.gz

```
## Avec Velero :
https://velero.io/docs/v1.7/basic-install/

- On Master node pgarciaipi_1 :

```bash
multipass shell pgarciaipi_1
sudo su
wget https://github.com/vmware-tanzu/velero/releases/download/v1.7.1/velero-v1.7.1-linux-amd64.tar.gz
tar -xvf velero-v1.7.1-linux-amd64.tar.gz
cd velero-v1.7.1-linux-amd64
mv velero /usr/local/bin
```
- Exemple Adhoc :

```bash
velero backup create nginx-backup --include-namespaces nginx-example
velero restore create --from-backup nginx-backup
```

- Exemple Cronjob :

Creates a backup that runs every day at 3am :

`velero schedule create example-schedule --schedule="0 3 * * *"`
